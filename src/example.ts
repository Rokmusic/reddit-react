
// 1
function strMethod (method: 'concat', first: string, second: string): string {
  switch (method) {
    case "concat": return first + ' ' + second;
  }
}

export default strMethod

// 2
const myHomeTask: IObj = {
  howIDoIt: "I Do It Wel",
  simeArray: ["string one", "string two", 42],
  withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],

};

interface IObjWithData {
  howIDoIt: string,
  simeArray: Array<string | number> }

interface IObj {
  howIDoIt: string,
  simeArray: Array<string | number>,
  withData: Array<IObjWithData>,
}

// 3

interface IMyArray<T> {
  [N: number]: T;
  reduce<U>(
    callback: (accumulator: U, value: T, currentIndex: number, array: Array<T>) => U,
    initialValue?: U
  ) : U
}

const initialValue: number = 0;

const testArr: IMyArray<number> = [1,2,3];

testArr.reduce((
  accumulator,
  value,
  currentIndex,
  array,
) => accumulator + value, initialValue);

// 4

interface IHomeTask {
  data: string;
  numbericData: number;
  date: Date;
  externalData: {
    basis: number;
    value: string;
  }
}

type MyPartial<T> = {
  [N in keyof T]?: T[N] extends object ? MyPartial<T[N]> : T[N]
}

const homeTask: MyPartial<IHomeTask> = {
  externalData: {
    value: 'win'
  }
}