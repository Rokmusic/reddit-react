import { createSlice } from "@reduxjs/toolkit";

const Initial = {
  commentText: 'Hello',
};

export const commentTextSlice = createSlice({
  name: 'commentText',
  initialState: Initial,
  reducers: {
    setText: (state, action) => void(state.commentText = action.payload)
  },
});

export const { setText } = commentTextSlice.actions;