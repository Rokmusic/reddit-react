import { createSlice } from "@reduxjs/toolkit";
import { fetchPostComments } from "../actions/fetchPostComments";
import { fetchPosts } from "../actions/fetchPosts";

interface IItemPostData {
  children: any[]
}

interface IPostData {
  data: IItemPostData;
  comments: [];
  postId: string;
  status: string;
  error: string;
  after: string;
  countLoads: number;
}

const initial: IPostData = {
  data: {children: []},
  comments: [],
  postId: '',
  status: '',
  error: '',
  after: '',
  countLoads: 0
};

export const postSlice = createSlice({
  name: 'post',
  initialState: initial,
  reducers: {
    setPostId: (state, action) => void(state.postId = action.payload),
    setComments: (state, action) => void(state.comments = action.payload),
    setCountLoads: (state, action) => void(state.countLoads = action.payload),
  },
  extraReducers(builder) {
    builder
      .addCase(fetchPostComments.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchPostComments.fulfilled, (state, action) => {
        state.status = 'idle';
        state.comments = action.payload
      })
      .addCase(fetchPostComments.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action!.error.message ? action!.error.message : ''
      })
      .addCase(fetchPosts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = 'idle';
        if (action.payload?.after) {
          state.after = action.payload.after;
        }

        if (action.payload?.children) {
          state.data.children = [...state.data.children, ...action.payload!.children];
        }

        state.countLoads = state.countLoads + 1;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action!.error.message ? action!.error.message : ''
      })
  }
});

export const { setPostId, setComments, setCountLoads } = postSlice.actions;