import { createSlice } from "@reduxjs/toolkit";
import { fetchUserData } from "../actions/fetchUserData";


type TUserDataItem = {
  name?: string;
  iconImg?: string;
}
interface IUserData {
  data: TUserDataItem;
  status: string;
  error: string
}

const Initial: IUserData = {
  data: {name: '', iconImg: ''},
  status: '',
  error: ''
};

export const userSlice = createSlice({
  name: 'commentText',
  initialState: Initial,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchUserData.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchUserData.fulfilled, (state, action) => {
        state.status = 'idle';
        if (action.payload) {
          const icon = action.payload.icon_img.split('?')[0];
          state.data = {name: action.payload?.name, iconImg: icon};
        }
      })
      .addCase(fetchUserData.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action!.error.message ? action!.error.message : ''
      })
  }
});

// export const { setUserName, setIconImg, setRequestStatus } = userSlice.actions;