import { createSlice } from "@reduxjs/toolkit";
import { saveToken } from "../actions/saveToken";

interface IInitialTokenData {
  token: string;
  status: string;
  error: string;
}

const Initial: IInitialTokenData = {
  token: '',
  status: '',
  error: ''
};

export const tokenSlice = createSlice({
  name: 'token',
  initialState: Initial,
  reducers: {},
  extraReducers(builder) {
    builder
      // .addCase(saveToken.pending, (state) => {
      //   state.status = 'loading';
      // })
      .addCase(saveToken.fulfilled, (state, action) => {
        state.status = 'idle';
        state.token = action.payload
      })
      // .addCase(saveToken.rejected, (state, action) => {
      //   state.status = 'failed';
      //   state.error = action!.error.message ? action!.error.message : ''
      // })
  }
});