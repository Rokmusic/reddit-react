import { configureStore } from "@reduxjs/toolkit";
import thunk  from 'redux-thunk'
import { tokenSlice } from "./reducers/tokenSlice";
import { commentTextSlice } from "./reducers/commentTextSlice";
import { postSlice } from "./reducers/postSlice";
import { userSlice } from "./reducers/userSlice";
import { useDispatch } from "react-redux";

export const store = configureStore({
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware()
    .prepend()
    .concat(thunk),
  reducer: {
    commentTextSlice: commentTextSlice.reducer,
    tokenSlice: tokenSlice.reducer,
    postSlice: postSlice.reducer,
    userSlice: userSlice.reducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>
export const useAppDispatch = () => useDispatch<AppDispatch>();
