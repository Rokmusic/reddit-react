import { createAsyncThunk } from "@reduxjs/toolkit";

export const saveToken = createAsyncThunk('tokenSlice/saveToken',  () => {
  let myToken: string = '';

  if (location?.hash?.length && location?.hash?.includes('access_token')) {
      myToken = location?.hash?.split('&')[0].split('=')[1];
    }

  return myToken
});
