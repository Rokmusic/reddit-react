import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

interface IResponseData {
  data: {children: [], after: string}
}

interface IFetchPostProps {
  token: string;
  after: string;
}

export const fetchPosts = createAsyncThunk('postSLice/fetchPosts', async ({token, after}: IFetchPostProps) => {
  const response = await axios.get('https://oauth.reddit.com/best.json?sr_detail=true',
    {
      params: {limit: 10, after: after},
      headers: {Authorization: `bearer ${token}`}
    });

  const userData: IResponseData = response.data;
  return userData.data
});