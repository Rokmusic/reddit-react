import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

interface IUserData {
  icon_img: string;
  name: string;
}

export const fetchUserData = createAsyncThunk('userSlice/fetchUserData', async (token: string) => {

  if (!token || token === 'undefined') return;
  const response = await axios.get('https://oauth.reddit.com/api/v1/me?raw_json=1',
    {
      headers: {Authorization: `bearer ${token}`}
    });

  const userData: IUserData = response.data;
  return userData
});