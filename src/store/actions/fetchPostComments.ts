import axios from "axios";
import {generateId} from "../../utils/react/generateRandomIndex";
import { createAsyncThunk } from "@reduxjs/toolkit";

interface IPropsFetchPostComments {
  token: string;
  postId: string;
}

export const fetchPostComments =
  createAsyncThunk('postSlice/fetchPostComments', async ({token, postId}: IPropsFetchPostComments) => {

  if (!token || token === 'undefined') return;
  const response = await axios.get(`https://oauth.reddit.com/comments/${postId}`,
    {
      params: { limit: 10 },
      headers: { Authorization: `bearer ${token}` }
    });

    const userData = response.data.map((item: { data: {children: []} }) => {
      if (item.data.children) {
        return item.data.children
      } else {
        return item.data
      }
    });

    const newData = userData.flat(2);
    return newData.map(generateId)
});