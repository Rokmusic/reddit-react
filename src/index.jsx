import App from "./App";
import * as React from 'react'
import { createRoot } from 'react-dom/client';
import {store} from "./store/store";
import {Provider} from "react-redux";

const container = document.getElementById('react_root');
const root = createRoot(container);
root.render(
    <Provider store={store}>
        <App />
    </Provider>
);
