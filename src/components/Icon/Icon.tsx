import React from "react";
import {BlockIcon} from "../icons/BlockIcon";
import {WarningIcon} from "../icons/WarningIcon";
import classNames from "classnames";
import styles from './icon.module.css'
import {CommentsIcon} from "../icons/CommentsIcon";
import {SaveIcon} from "../icons/SaveIcon";
import {ShareIcon} from "../icons/ShareIcon";


type TSizes = 32 | 16 | 8;
export enum EIcons {
  block = 'block',
  warning = 'warning',
  comments = 'comments',
  share = 'share',
  save = 'save',
}

const icons = {
  [EIcons.block]: <BlockIcon/>,
  [EIcons.warning]: <WarningIcon/>,
  [EIcons.comments]: <CommentsIcon/>,
  [EIcons.share]: <ShareIcon/>,
  [EIcons.save]: <SaveIcon/>,
};

interface IIconProps {
  name?: EIcons;
  size?: TSizes;
}

export function Icon({ name = EIcons.block, size = 8 }: IIconProps) {

  const classes = classNames(
    styles[`s${size}`],
  );

  return (
    <div className={`${styles.iconContainer} ${classes}`}>{icons[name]}</div>
  )
}