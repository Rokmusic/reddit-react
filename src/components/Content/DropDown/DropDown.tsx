import React, {useEffect, useState} from "react";
import ReactDOM from 'react-dom'
import styles from "./dropDown.module.css";

interface IDropDownProps {
  button: React.ReactNode;
  children: React.ReactNode;
  isOpen?: boolean;
  onOpen?: () => void;
  onClose?: () => void;
  domRect?: {top: number, left: number, height: number, width: number}
}

const NOOP = () => {};

export function DropDown({ button, children, isOpen, onOpen = NOOP, onClose = NOOP, domRect }: IDropDownProps) {
  const [isDropDownOpen, setIsDropDownOpen] = useState(isOpen);

  useEffect(() => setIsDropDownOpen(isOpen), [isOpen]);
  useEffect(() => isDropDownOpen ? onOpen() : onClose(), [isDropDownOpen]);

  const handleOpen = () => {
    if (isOpen === undefined) {
      setIsDropDownOpen(!isDropDownOpen)
    }
  };
  
  const node = document.querySelector('#dropDown_root');
  if (!node) return null;

  return (
    <div className={styles.container} >
      <div onClick={handleOpen} className={styles.menuButtonContainer}>
        { button }
      </div>
      {isDropDownOpen && (
        ReactDOM.createPortal((
          <div className={styles.dropDown} style={{
            top: Math.round(domRect ? (domRect.top + domRect.height - 10) : 0),
            left: Math.round(domRect ? (domRect.left + domRect.width - 100) : 0),
          }}>
            <div className={styles.menuItemsListContainer} onClick={() => setIsDropDownOpen(false)}>
              {children}
            </div>
          </div>
        ), node)
      )}
    </div>
  )
}