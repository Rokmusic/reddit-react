import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import styles from "./post.module.css";
import {CommentForm} from "../CommentForm/CommentForm";
import {PostComments} from "../PostComments/PostComments";
import { useDispatch, useSelector } from "react-redux";
import { setComments, setPostId } from "../../../store/reducers/postSlice";
import { RootState } from "../../../store/store";
import { useNavigate } from "react-router-dom";

export function Post() {
  const ref = React.useRef<HTMLDivElement>(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const comments = useSelector((store: RootState) => store.postSlice.comments);


  const node = document.querySelector('#modal_root');
  if (!node) return null;

  useEffect(() => {
    return () => {
      dispatch(setComments([]));
      dispatch(setPostId(''));
    }
  }, [navigate]);

  useEffect(() => {

    function handleClick(event: MouseEvent) {
      if (event.target instanceof Node && !node?.contains(event.target)) {
        navigate('/posts');
      }
    }
    document.addEventListener('mouseup', handleClick);
    return () => {
      document.removeEventListener('mouseup', handleClick);
    }
  }, [ref]);


  return ReactDOM.createPortal((
    <div className={styles.modal} ref={ref}>
      <h2>123</h2>
      <div className={styles.content}>
        <p>3123</p>
        <p>3123</p>
        <p>3123</p>
        <p>3123</p>
      </div>
      <CommentForm controlled={true}/>
      {comments?.length
        ? <PostComments dataComments={comments} /> : <div>Загрузка</div>
      }
    </div>
  ), node)
}