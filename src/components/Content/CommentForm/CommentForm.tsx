import React, { useEffect, useRef, } from "react";
import styles from "./commentForm.module.css";
import { useDispatch, useSelector } from "react-redux";
import { setText } from "../../../store/reducers/commentTextSlice";
import { RootState } from "../../../store/store";
import { Field, Form, Formik, FormikValues } from "formik";

interface ICommentsFormProps {
  controlled: boolean;
  fromComments?: boolean;
  textComment?: string;
  handleChangeOfComments?: (comment: string) => void;
}

export function CommentForm({controlled, fromComments, textComment, handleChangeOfComments }: ICommentsFormProps) {
  const ref = useRef<HTMLTextAreaElement>(null);
  const dispatch = useDispatch();

  const value = useSelector((store: RootState) => store.commentTextSlice.commentText);

  useEffect(() => {
    if (ref.current) {
      ref.current.selectionStart = ref.current.selectionEnd = ref.current.value.length;
      ref.current.focus()
    }
  }, [fromComments]);

  function handleChangeText(comment: string) {
    dispatch(setText(comment));
  }

  const changerCommentsValues = (values: FormikValues) => {
    if (controlled) {
      if (fromComments) {
        if (handleChangeOfComments) {
          handleChangeOfComments(values.comment)
        }
      } else {
        handleChangeText(values.comment);
      }
    } else {
      noop();
    }
  };

  const noop = () => {};

  return (
    <Formik
      initialValues={{
        comment: controlled
          ? fromComments
            ? textComment : value
          : undefined
      }}
      onSubmit={() => {console.log(`Форма отправлена`)}}
    >
      {({values}) => {
        return (
          <Form className={styles.form} onChange={() => changerCommentsValues(values)} >
            <Field as="textarea" name='comment' innerRef={ref} className={styles.input}/>
            <button type={'submit'} className={styles.button}>Комментировать</button>
          </Form>
        )}}
    </Formik>
  )
}