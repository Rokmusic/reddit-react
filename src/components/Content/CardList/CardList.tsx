import React, { useEffect, useRef, useState } from "react";
import { Card }        from "./Card/Card";
import { merge }       from "../../../utils/js/merge";
import { generateId }  from "../../../utils/react/generateRandomIndex";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "../../../store/store";
import { fetchPosts } from "../../../store/actions/fetchPosts";
import { setCountLoads } from "../../../store/reducers/postSlice";
import { useNavigate } from "react-router-dom";

export function CardList() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const token = useSelector((store: RootState) => store.tokenSlice.token);
  const postsInState = useSelector((store: RootState) => store.postSlice.data);
  const status = useSelector((store: RootState) => store.postSlice.status);
  const errorLoading = useSelector((store: RootState) => store.postSlice.error);
  const nextAfter = useSelector((store: RootState) => store.postSlice.after);
  const countLoads = useSelector((store: RootState) => store.postSlice.countLoads);
  const bottomOfList = useRef<HTMLDivElement>(null);

  const [canLoadFromButton, setCanLoadFromButton] = useState(false);

  useEffect(() => {
    if (countLoads === 3) {
      setCanLoadFromButton(true);
    } else {
      setCanLoadFromButton(false);
    }
  }, [countLoads]);

  useEffect(() => {

    const observer = new IntersectionObserver((entries) =>
      {
        if (entries[0].isIntersecting && countLoads < 3 && token) {
          dispatch(fetchPosts({token: token, after: nextAfter}))
        }
      },{rootMargin: '10px'});

    if (bottomOfList.current) {
      observer.observe(bottomOfList.current)
    }

    return () => {
      if (bottomOfList.current) {
        observer.unobserve(bottomOfList.current)
      }
    }

  }, [bottomOfList.current, nextAfter, token, countLoads, navigate]);

  const load = async () => {
    await (fetchPosts({token: token, after: nextAfter}));
    dispatch(setCountLoads(0))
  };

  const handleItemClick = (id: string) => {
    if (id) {
      // console.log(id);
    }
  };

  return (
    <>
      {!errorLoading.length && status === 'idle' && !postsInState?.children.length &&
          <div className={'statuses'}>Нет ни одного поста</div>
      }

      {postsInState &&
          <Card list={postsInState?.children.map(generateId).map(merge({ onClick: handleItemClick }))} />
      }

      <div ref={bottomOfList}></div>

      {errorLoading &&
        <div className={'statuses'}>{errorLoading}</div>
      }

      {status === 'loading' && !canLoadFromButton &&
          <div className={'statuses'}>Загрузка</div>
      }

      {canLoadFromButton &&
          <button className={'button-center'} onClick={load}>Загрузить ещё</button>
      }
    </>
  )
}