import React from "react";
import styles from "./card.module.css";
import {MetaData} from "./components/TextContent/MetaData/MetaData";
import {PostTitle} from "./components/PostTitle/PostTitle";
import {Preview} from "./components/Preview/Preview";
import {CardMenu} from "./components/CardMenu/CardMenu";
import {TextContent} from "./components/TextContent/TextContent";
import {Controls} from "./components/Controls/Controls";
import {KarmaCounter} from "./components/Controls/KarmaCounter/KarmaCounter";
import {CommentsButton} from "./components/Controls/CommentsButton/CommentsButton";
import {Actions} from "./components/Controls/Actions/Actions";

interface IItem {
  id?: string;
  onClick: (id: string) => void;
  classname?: string;
  data?: IDataItem;
  comments?: []
}

interface IDataItem {
  title: string
  thumbnail: string
  author: string;
  num_comments: number;
  score: number
  created_utc: number;
  id: string;
}

interface ICardProps {
  list: IItem[]
}

const noop = () => {};

export function Card({list}: ICardProps) {

  return (
    <ul className={styles.cardList}>
      {list.map
      (({
          classname,
          onClick = noop,
          id = '',
          data,
        }) => (
        (id &&
          <li key={id} className={styles.card} onClick={() => {
            onClick(data!.id)

          }} >
            <TextContent>
              <MetaData imageSrc={data!.thumbnail} userName={data!.author} created_utc={data!.created_utc} />
              <PostTitle title={data!.title} postId={data!.id} />
            </TextContent>
            <Preview />
            <CardMenu />
            <Controls>
              <KarmaCounter score={data!.score}/>
              <CommentsButton numComments={data!.num_comments}/>
              <Actions/>
            </Controls>
          </li>)
      ))}
    </ul>

  )
}