import styles from "./metaData.module.css";
import React from "react";
import {UserInfo} from "./UserInfo/UserInfo";
import {CreateTime} from "./CreateTime/CreateTime";

interface IMetaDataProps {
  imageSrc: string;
  userName: string;
  created_utc: number
  fromComments?: boolean;
}

export function MetaData({imageSrc, userName, created_utc, fromComments}: IMetaDataProps) {
  return (
    <div className={styles.metaData + ` ${fromComments ? styles.reverse : ''}`}>
      <UserInfo imageSrc={imageSrc} userName={userName} fromComments={fromComments} />
      <CreateTime created_utc={created_utc} fromComments={fromComments} />
    </div>
  )
}