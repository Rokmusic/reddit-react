import styles from "./userInfo.module.css";
import React from "react";
import { Link } from "react-router-dom";

interface IUserInfoProps {
  imageSrc?: string;
  userName: string;
  fromComments?: boolean;
}

export function UserInfo ({imageSrc, userName, fromComments}: IUserInfoProps) {
  return (
    <div className={styles.metaData}>
      <div className={styles.userLink}>
        <img
          className={styles.avatar}
          src={imageSrc}
          alt={'avatar'}
        />
        <Link to={"/user"} className={styles.userName}>{userName}</Link>
        {fromComments &&
          <div className={styles.league}>Лига Юристов</div>
        }
      </div>
    </div>
  )
}