import styles from "./сreateTime.module.css";
import React from "react";
import moment from "moment";

interface ICreateTimeProps {
  created_utc: number;
  fromComments?: boolean;
}

export function CreateTime({created_utc, fromComments}: ICreateTimeProps) {
  const getDate = (created_utc: number) => {
    const dateNow = new Date().getTime();
    const differenceOfDate = dateNow / 1000 - created_utc;
    if (differenceOfDate > 43399 || fromComments) {
      const hours = `${new Date(differenceOfDate * 1000).getHours() - 1}`.slice(-2);
      const time = `${hours} ${Number(hours) < 5 ? 'часа' : 'часов'}`;

      return time + ' назад';
    } else {
      return created_utc ? moment(created_utc * 1000).format("DD MMMM yyyy") : '...';
    }
  };

  return (
    <span className={styles.createdAt}>
      <span className={styles.publishedLabel}>опубликовано </span>
      {getDate(created_utc)}
    </span>
  )
}