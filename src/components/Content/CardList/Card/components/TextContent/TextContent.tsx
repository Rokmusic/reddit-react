import styles from "./textContent.module.css"
import React from "react";

interface IPorpsTextContent {
  children?: React.ReactNode
}

export function TextContent({ children }: IPorpsTextContent) {
  return (
    <div className={styles.textContent}>
      {children}
    </div>
  )
}