import React from "react";
import {EColors, Text} from "../../../../../../Text/Text";

interface IMenuButtonItemProps {
  text: string;
  children?: React.ReactNode;
}

export function MenuButtonItem({text, children}: IMenuButtonItemProps) {
  return (
    <>
      {children}
      <Text size={12} desktopSize={16} tabletSize={14} color={EColors.grey66}>{text}</Text>
    </>
  )
}