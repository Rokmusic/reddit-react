import styles             from "./cardMenu.module.css";
import React, {
  useEffect, useRef, useState
}                         from "react";
import { DropDown }       from "../../../../DropDown/DropDown";
import { GenericList }    from "../../../../../GenericList/GenericList";
import { merge }          from "../../../../../../utils/js/merge";
import { generateId }     from "../../../../../../utils/react/generateRandomIndex";
import MenuButton         from "../../../../../buttons/MenuButton/MenuButton";
import { CloseButton }    from "../../../../../buttons/CloseButton/CloseButton";
import { MenuButtonItem } from "./MenuButton/MenuButtonItem";
import { EIcons, Icon }   from "../../../../../Icon/Icon";

const LIST = [
  {
    As: 'li' as const,
    element: <MenuButtonItem text={'Комментарии'} children={<Icon name={EIcons.comments} size={16} />}/>,
    classname: 'desktopOnly',
  },
  {
    As: 'li' as const,
    element: <MenuButtonItem text={'Поделиться'} children={<Icon name={EIcons.share} size={16} />}/>,
    classname: 'desktopOnly',
  },
  {
    As: 'li' as const,
    element: <MenuButtonItem text={'Скрыть'} children={<Icon name={EIcons.block} size={16} />}/>,
    icon: <Icon name={EIcons.block} size={16} />
  },
  {
    As: 'li' as const,
    element: <MenuButtonItem text={'Сохранить'} children={<Icon name={EIcons.save} size={16} />}/>,
    classname: 'desktopOnly',
  },
  {
    As: 'li' as const,
    element: <MenuButtonItem text={'Пожаловаться'} children={<Icon name={EIcons.warning} size={16} />}/>,
  },
].map(generateId);

export function CardMenu() {
  const ref = useRef<HTMLDivElement>(null);
  const [list, setList] = useState(LIST);
  const [domRect, setDomRect] = useState({top: 0, left: 0, height: 0, width: 0});

  const handleItemClick = (id: string) => {
    console.log(id);
  };

  useEffect(() => {
    if (ref.current) {
      const rect = ref.current.getBoundingClientRect()
      setDomRect(rect);
    }
  }, [DropDown]);

  return (
    <div className={styles.menu} id={'menu'} ref={ref}>
      <DropDown isOpen={undefined} button={MenuButton} domRect={domRect}>
          <GenericList list={list.map(merge({ onClick: handleItemClick }))}/>
          <CloseButton/>
      </DropDown>
    </div>
  )
}