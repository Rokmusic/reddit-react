import styles from "./controls.module.css";
import React from "react";

interface IPropsControl {
  children?: React.ReactNode
}

export function Controls({ children }: IPropsControl) {
  return (
    <div className={styles.controls}>
      {children}
    </div>
  )
}