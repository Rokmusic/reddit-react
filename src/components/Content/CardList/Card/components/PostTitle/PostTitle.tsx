import styles from "./postTitle.module.css";
import React from "react";
import { useDispatch } from "react-redux";
import { setPostId } from "../../../../../../store/reducers/postSlice";
import { Link } from "react-router-dom";

interface IPostProps {
  title?: string;
  postId: string;
  comments?: [];
}

export function PostTitle({ title, postId }: IPostProps) {
  const dispatch = useDispatch();

  function handleOnClick() {
    dispatch(setPostId(postId));
  }

  return (
    <h2 className={'title'}>
      <Link to={`/posts/${postId}`} className={styles.postLink} onClick={handleOnClick}>
        {title || 'post-title'}
      </Link>

    </h2>
  )
}