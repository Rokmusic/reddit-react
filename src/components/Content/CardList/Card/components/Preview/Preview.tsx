import styles from "./preview.module.css";
import React from "react";

export function Preview() {
  return (
    <div className={styles.preview}>
      <img
        className={styles.previewImg}
        src={'https://cdn.dribbble.com/userupload/4160954/file/original-43e95bbb48627c93c090f016241e7c5e.jpg'}
      />
    </div>
  )
}