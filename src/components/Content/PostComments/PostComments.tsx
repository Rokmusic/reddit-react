import React, { useState } from "react";
import styles from "./PostComments.module.css";
import { CommentForm } from "../CommentForm/CommentForm";
import { MetaData } from "../CardList/Card/components/TextContent/MetaData/MetaData";

interface IPostCommentsProps {
  dataComments: Array<IDataArr>[]
}

type IDataArr = {
  id: string
  kind: string;
  data: IItemData;
  replies?: any;
}

interface IItemData {
  body: string;
  replies: {data: {children: [], depth?: number}, };
  depth: number;
  created_utc: number;
  author: string;
}

export function PostComments({dataComments}: IPostCommentsProps) {
  const dataArr = dataComments.flat(2);

  function getCommentsBlock(body: string, key: string, item: IDataArr, isChildren: boolean) {
    const [isOpenComments, setIsOpenComments] = useState(false);
    const depth = [item].map(item => item.data.depth);
    const dataRepliesChildren: IDataArr[] = item?.data.replies?.data?.children;

    const [textComment, setTextComment] = useState('');

    function handleChange(comment: string) {
      setTextComment(comment);
    }

    const handlerClick = (onChange: React.Dispatch<React.SetStateAction<boolean>>, status: boolean, author: string) => {
      onChange(!status);
      setTextComment(author + ', ' +
      (textComment.length ? textComment.split(`${author + ', '}`, -1)[1] : ''));
    };

    return (
      <div className={styles.containerComments}
           style={{paddingLeft: depth[0] * 20 + 10}} key={isChildren ? key + "1" : key}
      >
        <MetaData imageSrc={'https://b.thumbs.redditmedia.com/Sd5CvsONpfVKzhKzXR2MQzkIu1ONJZCIhm1n8mfsHco.jpg'}
                  userName={item.data.author} created_utc={item.data.created_utc} fromComments={true}
        />
        <div className={styles.bodyComment}>{body}</div>
        <div className={styles.containerCommentsButtons}>
          <button key={key}
                  onClick={() => handlerClick(setIsOpenComments, isOpenComments, item.data.author)}
          >
            Ответить
          </button>
          <button>Поделиться</button>
          <button>Пожаловаться</button>
        </div>
        {isOpenComments &&
          <CommentForm
            controlled={true}
            fromComments={true}
            textComment={textComment}
            handleChangeOfComments={handleChange}
          />
        }
        {dataRepliesChildren &&
          getComments(dataRepliesChildren, true)
        }
      </div>
    )
  }

  function getComments (arr: IDataArr[], isChildren: boolean) {
    if (arr) {
      return arr.map((item, index) => {
        if (item?.kind === 't1') {
          return getCommentsBlock(item?.data.body, isChildren ? index.toString() : item.id, item, isChildren);
        }
      })
    }
  }

  return (
    <>
      {getComments(dataArr, false)}
    </>
  )
}