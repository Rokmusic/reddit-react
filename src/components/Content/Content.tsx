import styles from "./content.module.css";
import React, {ReactNode} from "react";

interface IContentProp {
  children?: ReactNode;
}

export function Content({ children }: IContentProp) {
  return (
    <main className={styles.content}>
      {children}
    </main>
  )
}