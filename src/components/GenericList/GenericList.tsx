import React from "react";
import styles from "./genericList.module.css";

interface IItem {
  id: string;
  onClick: (id: string) => void;
  classname?: string;
  As?: 'li' | 'a';
  href?: string;
  element: React.ReactNode
}

interface IGenericListProps {
  list: IItem[]
}

const noop = () => {};

export function GenericList({ list }: IGenericListProps) {

  return (
    <ul className={styles.menuItemsList}>
      {list.map
        (({
          As = 'li',
          classname,
          onClick = noop,
          id,
          element,
        }) => (
            <As
              className={classname ?
                styles[classname] + ` ${styles.menuItem}` : `${styles.menuItem}`}
              onClick={() => onClick(id)}
              key={id}
            >
              {element}
            </As>
        ))}
    </ul>
  )
}