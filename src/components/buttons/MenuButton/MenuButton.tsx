import styles from "../../Content/CardList/Card/components/CardMenu/cardMenu.module.css";
import React from "react";
import {MenuIcon} from "../../icons/MenuIcon";

const menuButton =
  <button className={styles.menuButton}>
    <MenuIcon/>
  </button>;

export default menuButton
