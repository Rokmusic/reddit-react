import styles from "./closeButton.module.css";
import {EColors, Text} from "../../Text/Text";
import React from "react";

export function CloseButton() {
  return (
    <button className={styles.closeButton}>
      <Text mobileSize={12} size={14} color={EColors.grey66}>
        Закрыть
      </Text>
    </button>
  )
}