import React from "react";
import styles from "./userBlock.module.css";
import {IconAnon} from "../../../icons/IconAnon";
import {EColors, Text} from "../../../Text/Text";

interface IUserBlockProps {
  avatarSrc?: string;
  username?: string;
}

export function UserBlock({ avatarSrc, username}: IUserBlockProps) {
  return (
    <a
      className={styles.userBox}
      href='https://www.reddit.com/api/v1/authorize?client_id=ckqbhdgQJn0a1gsz5cLS6Q&response_type=token&state=random_string&redirect_uri=http://localhost:3333/&scope=read submit identity'>
      <div className={styles.avatarBox}>
        {avatarSrc
          ? <img src={avatarSrc} alt={"user avatar"} className={styles.avatarImage}/>
          : <IconAnon/>
        }
      </div>

      <div className={styles.username}>
        {/*<Breack/>*/}
        <Text size={20} color={username ? EColors.black : EColors.grey99}>{username || 'Аноним'}</Text>
      </div>
    </a>
  )
}