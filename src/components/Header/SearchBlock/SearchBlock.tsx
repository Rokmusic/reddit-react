import styles from './searchBlock.module.css'
import React from 'react'
import {UserBlock} from "./UserBlock/UserBlock";
import { useSelector } from "react-redux";
import { RootState } from "../../../store/store";

export function SearchBlock () {
  const data = useSelector((store: RootState) => store.userSlice.data);

  return (
    <div className={styles.searchBlock}>
      <UserBlock avatarSrc={data.iconImg} username={data.name}/>
    </div>
  )
}