import * as React from 'react';
import Header from './components/Header/Header';
import { Layout } from "./components/Layout/Layout";
import './main.global.css'
import { Content } from "./components/Content/Content";
import { CardList } from "./components/Content/CardList/CardList";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "./store/store";
import { Route, Routes, Navigate, BrowserRouter } from "react-router-dom";
import { Post } from "./components/Content/Post/Post";
import { ErrorNotFoundPage } from "./components/ErrorNotFoundPage/ErrorNotFoundPage";
import { useEffect } from "react";
import { saveToken } from "./store/actions/saveToken";
import { fetchUserData } from "./store/actions/fetchUserData";
import { fetchPostComments } from "./store/actions/fetchPostComments";

function App () {
  const dispatch = useAppDispatch();
  const token = useSelector((store: RootState) => store.tokenSlice.token);
  const postId = useSelector((store: RootState) => store.postSlice.postId);
  const name = useSelector((store: RootState) => store.userSlice.data.name);

  useEffect(() => {
    if (!name?.length) dispatch(fetchUserData(token));
  }, [token]);

  useEffect(() => {
    if (postId.length) dispatch(fetchPostComments({token, postId}));
  }, [postId]);

  useEffect(() => {
    if (!token?.length) dispatch(saveToken());
  }, []);

  return (
    <BrowserRouter>
      <Layout>
        <Header />
        <Content>
          <Routes >
            <Route path={'/'} element={token.length ? <Navigate to="/posts" /> : null}/>
            <Route path={'/posts/*'} element={
              <>
                <CardList />
                <Routes>
                  <Route path={'/:id'} element={<Post/>}/>
                </Routes>
              </>
            } />
            <Route path={'*'} element={<ErrorNotFoundPage />}/>
          </Routes>
        </Content>
      </Layout>
    </BrowserRouter>
  )
}

export default App