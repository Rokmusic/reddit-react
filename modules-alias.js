const path = require('path');

module.exports = [
    {resolve: {alias: {'@Components': path.resolve(__dirname, './src/common/components/')}}},
    {resolve: {alias: {'@Utils': path.resolve(__dirname, './src/utils/')}}},
    {resolve: {alias: {'@Slices': path.resolve(__dirname, './src/slices/')}}}
];